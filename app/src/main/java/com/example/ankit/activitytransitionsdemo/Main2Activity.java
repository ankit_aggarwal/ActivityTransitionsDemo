package com.example.ankit.activitytransitionsdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inside your activity (if you did not enable transitions in your theme)
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        setContentView(R.layout.activity_main2);

        ImageView imageView1 = (ImageView) findViewById(R.id.iv_circular_reveal1);
        Picasso.with(this)
                .load(R.drawable.image)
                .resize(400, 400)
                .into(imageView1);

        ImageView imageView2 = (ImageView) findViewById(R.id.iv_circular_reveal2);
        Picasso.with(this)
                .load(R.drawable.image2)
                .resize(400, 400)
                .into(imageView2);
    }
}
