package com.example.ankit.activitytransitionsdemo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    private ImageView mImageView1;
    private ImageView mImageView2;

    private boolean hidden = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inside your activity (if you did not enable transitions in your theme)
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        setContentView(R.layout.activity_main);

        mImageView1 = (ImageView) findViewById(R.id.iv_circular_reveal1);
        mImageView2 = (ImageView) findViewById(R.id.iv_circular_reveal2);
        Button buttonReveal = (Button) findViewById(R.id.btn_reveal);
        Button buttonStart = (Button) findViewById(R.id.btn_open_activity);

        Picasso.with(this)
                .load(R.drawable.image)
                .resize(320, 320)
                .into(mImageView1);

        Picasso.with(this)
                .load(R.drawable.image2)
                .resize(320, 320)
                .into(mImageView2);

        // define a click listener
        mImageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                // create the transition animation - the images in the layouts
                // of both activities are defined with android:transitionName="transition"
                Pair<View, String> pair1 = new Pair<>((View) mImageView1, "transition1");
                Pair<View, String> pair2 = new Pair<>((View) mImageView1, "transition2");
                ActivityOptionsCompat options = ActivityOptionsCompat
                        .makeSceneTransitionAnimation(MainActivity.this, pair1, pair2);
                // start the new activity
                startActivity(intent, options.toBundle());
            }
        });

        if (buttonReveal != null) {
            buttonReveal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (hidden) {
                        showView();
                        hidden = false;
                    } else {
                        hideView();
                        hidden = true;
                    }
                }
            });
        }

        if (buttonStart != null) {
            buttonStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, Main22Activity.class),
                            ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this).toBundle());
                }
            });
        }
    }

    private void hideView() {
        // get the center for the clipping circle
        int cx = mImageView1.getWidth() / 2;
        int cy = mImageView1.getHeight() / 2;

        // get the initial radius for the clipping circle
        float initialRadius = (float) Math.hypot(cx, cy);

        // create the animation (the final radius is zero)
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(mImageView1, cx, cy, initialRadius, 0);
        }

        if (anim != null) {
            // make the view invisible when the animation is done
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mImageView1.setVisibility(View.INVISIBLE);
                }
            });

            // start the animation
            anim.start();
        }
    }

    private void showView() {
        // get the center for the clipping circle
        int cx = mImageView1.getWidth() / 2;
        int cy = mImageView1.getHeight() / 2;

        // get the final radius for the clipping circle
        float finalRadius = (float) Math.hypot(cx, cy);

        // create the animator for this view (the start radius is zero)
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(mImageView1, cx, cy, 0, finalRadius);
        }

        // make the view visible and start the animation
        mImageView1.setVisibility(View.VISIBLE);

        if (anim != null) {
            anim.start();
        }
    }
}
